package com.infodales.aem.test.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = PageLaceContentFragmentModel.class )

public class PageLaceContentFragmentModel {
    @Inject
    List<String> link;

    List<PageLaceContentFragmentPojo> list;

    public List<PageLaceContentFragmentPojo> getList() {
        return list;
    }

    @Inject
    ResourceResolver resourceResolver;

    @PostConstruct

    public void init(){
        if (link != null){
            list = new ArrayList<PageLaceContentFragmentPojo>();
           for(String links:link){
               PageLaceContentFragmentPojo cf = new PageLaceContentFragmentPojo();
               Resource resource = resourceResolver.getResource(links);
               ContentFragment contentFragment = resource.adaptTo(ContentFragment.class);
               cf.setQuery(contentFragment.getElement("query").getContent().toString());
               cf.setAnswer(contentFragment.getElement("answer").getContent().toString());
               list.add(cf);
           }
        }
           }
        }



