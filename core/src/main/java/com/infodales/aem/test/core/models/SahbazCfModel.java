package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class)
public class SahbazCfModel {

    private String test;

    protected void init(){

        test="Hello Nature";
    }

    public String getTest() {
        return test;
    }
}
