package com.infodales.aem.test.core.servlets;

import com.infodales.aem.test.core.service.ApiConfigIDMediaService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths="+"/bin/checkMyMessage"
        })
public class DemoServlet extends SlingAllMethodsServlet {

        @OSGiService
        ApiConfigIDMediaService apiConfigIDMediaService;

        @Override
        protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
                String message=request.getParameter("message");

                apiConfigIDMediaService.getProtocol();


                HttpSession session = request.getSession();
                session.setAttribute("username",message);
                session.setAttribute("token","12345");

                response.getWriter().write("Response : "+message);
        }

        protected void doPost(final SlingHttpServletRequest request,final SlingHttpServletResponse reponse) throws IOException {
                String message;
                message = request.getParameter("message");
                reponse.getWriter().write(message);

        }



}
