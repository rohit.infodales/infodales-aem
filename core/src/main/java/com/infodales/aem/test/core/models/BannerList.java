package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BannerList {

    @Inject
    private String fileReference;

    @Inject
    private String link;

    public String getFileReference() {
        return fileReference;
    }

    public String getLink() {
        return link;
    }
}
