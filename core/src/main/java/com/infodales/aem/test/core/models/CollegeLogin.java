package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;


import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class CollegeLogin {
    @Inject
    @Optional
    private String description;
    @Inject
    @Optional
    private String title;

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
