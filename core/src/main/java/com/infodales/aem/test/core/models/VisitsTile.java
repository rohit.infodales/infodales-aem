package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class VisitsTile {

    @Inject
    private String fileReference;
    @Inject
    private String link;
    @Inject
    private String title;
    @Inject
    private String date;
    public String getFileReference() {
        return fileReference;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }
}
