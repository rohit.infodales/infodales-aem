
package com.infodales.aem.test.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths="+ "/bin/info/profile"
        })
public class ProfilegetServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse res) throws IOException {

        String value="";
        HttpSession session = req.getSession();
        if(session != null){
            Object sessionobj = session.getAttribute("name");
           if(sessionobj != null){
               value = sessionobj.toString();
           }else {
               value = "session is Empty";
           }
        }else{
            value = "Session is not create";
        }
        res.getWriter().write(value);


//        HttpSession session = req.getSession();
//        String name = (String)session.getAttribute("name");
//
//        PrintWriter out = res.getWriter();
//        out.println("Result is" + name + value);
//        System.out.println("Profileget is called");


//
//     String userid = request.getParameter("userid");
//        response.getWriter().write(profileService.getUserProfileById(userid).toString());
    }
}
