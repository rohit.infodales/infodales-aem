package com.infodales.aem.test.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(service = Servlet.class,
property = {
"sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.methods=" + HttpConstants.METHOD_POST,
 "sling.servlet.paths="+ "/bin/info/setsession"
})
public class ProfilesetServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse res) throws IOException {

        String name = req.getParameter("name");
        HttpSession session = req.getSession();
        session.setAttribute("name",  name);
        res.getWriter().write("login successfull");


//        res.sendRedirect("Profileget");


//        String name = session.getAttribute("name");
//        int value = Integer.parseInt(session.getAttribute(value));
//
//     String userid = request.getParameter("userid");
//        response.getWriter().write(profileService.getUserProfileById(userid).toString());
    }
}
