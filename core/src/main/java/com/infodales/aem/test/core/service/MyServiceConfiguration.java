package com.infodales.aem.test.core.service;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "My Service Configuration", description = "Service Configuration")
public @interface MyServiceConfiguration {

    @AttributeDefinition(name = "Config Value", description = "Configuration value")
    String configValue() default "none";

    @AttributeDefinition(name = "MultipleValues", description = "Multi Configuration values")
    String[] getStringValues() default {"no description","no value"};

    @AttributeDefinition(name = "NumberValue", description = "Number values", type=AttributeType.INTEGER)
    int getNumberValue() default 5;

}