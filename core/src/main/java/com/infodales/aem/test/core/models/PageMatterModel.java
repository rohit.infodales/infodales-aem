package com.infodales.aem.test.core.models;

import com.infodales.aem.test.core.service.MySimpleService;
import com.infodales.aem.test.core.service.MySimpleServiceImpl;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.osgi.service.component.annotations.Reference;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class PageMatterModel {

    @Reference
    MySimpleService simpleService;




    @Inject
    private String title;
    @ValueMapValue
    private String description;
    @Inject
    private String fileReference;

    @Inject
    List<Multi> myMultifield;

    @Inject
    List<Emii> myMultimeter;


    public List<Multi> getMyMultifield() {
        return myMultifield;
    }

    public List<Emii> getMyMultimeter() {
        return myMultimeter;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getFileReference() {
        return fileReference;
    }


String configValue;
    String[] getStringValues;
    int getNumberValue;

    public String getConfigValue() {
        return configValue;
    }



    @PostConstruct
    protected void init(){
        configValue=simpleService.getValue();

    }
}
